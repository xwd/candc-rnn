const char *PROGRAM_NAME = "rnn_super";

#include "base.h"
#include "main.h"

#include "config/config.h"
#include "io/format.h"
#include "config/format.h"

#include "io/reader.h"
#include "io/reader_factory.h"
#include "io/writer.h"
#include "io/writer_factory.h"

#include "model/model.h"

#include "timer.h"

#include "rnn_tagger.h"

using namespace std;
using namespace NLP;
using namespace NLP::IO;
using namespace NLP::Taggers;
using namespace NLP::CCG;

int
run(int argc, char** argv){

  Config::Main cfg(PROGRAM_NAME);
  Config::Op<bool> train(cfg, "train", "training (ture/false)", false);
  Config::Op<std::string> input_file(cfg, NLP::Config::SPACE, "input", "the input file to read from", STDIN);
  Config::Op<std::string> input_file_dev(cfg, NLP::Config::SPACE, "input_dev", "the input file to read from", STDIN);
  Config::Op<IO::Format> ifmt(cfg, "ifmt", "the input file format", Format("%w|%p|%s \n"));
  Config::Op<std::string> model(cfg, "model", "the dir to save the resulting model", "./");
  Config::Op<std::string> emb_file(cfg, "emb", "file name of the embeddings file (*UNKONWN* changed to ***PADDING***, otherwise untouched)", "");
  Config::Op<std::string> suf_file(cfg, "suf", "file name of the suffix file", "");
  Config::Op<std::size_t> de(cfg, "de", "embedding dimension", 50);
  Config::Op<std::size_t> bs(cfg, "bptt_bs", "bptt steps", 9);
  Config::Op<bool> dropout(cfg, "dropout", "use dropout", true);
  Config::Op<bool> verbose(cfg, "verbose", "use dropout", true);
  Config::Op<double> dropout_prob(cfg, "dropout_prob", "drop out success prob", 0.75);
  Config::Op<double> lr(cfg, "lr", "learning rate", 0.0025);
  Config::Op<double> nh(cfg, "nh", "hidden layer size", 200);

  cfg.parse(argc, argv);
  cfg.check();


  if (!train()) {

    std::string wx_mat;
    std::string wh_mat;
    std::string wy_mat;
    std::string emb_mat;
    std::string suf_mat;
    std::string cap_mat;

   // RnnTagger rnn_super(model(), emb_file(), suf_file());

    //########## evaluating all models
//    for (size_t i = 0; i < 30; ++i) {
//      cerr << i << endl;
//      wx_mat = model() + "/wx." + to_string(i) + ".mat";
//      wh_mat = model() + "/wh." + to_string(i) + ".mat";
//      wy_mat = model() + "/wy." + to_string(i) + ".mat";
//      emb_mat = model() + "/emb." + to_string(i) + ".mat";
//      suf_mat = model() + "/suffix." + to_string(i) + ".mat";
//      cap_mat = model() + "/cap." + to_string(i) + ".mat";
//
//      rnn_super.re_init_mats(wx_mat, wh_mat, wy_mat, emb_mat, suf_mat, cap_mat);
//      ReaderFactory reader(input_file(), ifmt());
//      rnn_super.tag_file(reader, true);
//    }

    //########## testing

    RnnTagger rnn_super(model(), emb_file(), suf_file(), dropout_prob(), dropout());
    ReaderFactory reader(input_file(), ifmt());
    rnn_super.eval_mtag(reader);


  } else {

    //########## training


    //Stopwatch watch;

    string emb_file_name = model() + "/" + emb_file();
    string suf_file_name = model() + "/" + suf_file();

    RnnTagger rnn_super(de(), 5, 5, 7, nh(), bs(), emb_file_name, suf_file_name, model(),
                        true, 50, dropout(), dropout_prob(), verbose(), lr());

    ReaderFactory reader(input_file(), ifmt());
    ReaderFactory reader_dev(input_file_dev(), ifmt());

    cerr << "training started\n";
    rnn_super.train(reader, reader_dev);

  }

  return 0;

}

