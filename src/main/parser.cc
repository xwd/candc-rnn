// C&C NLP tools
// Copyright (c) Universities of Edinburgh, Oxford and Sydney
// Copyright (c) James R. Curran
//
// This software is covered by a non-commercial use licence.
// See LICENCE.txt for the full text of the licence.
//
// If LICENCE.txt is not included in this distribution
// please email candc@it.usyd.edu.au to obtain a copy.

#include "base.h"

#include "config/config.h"

#include "pool.h"

#include "model/model.h"

#include "io/format.h"
#include "config/format.h"

#include "io/reader.h"
#include "io/writer.h"

#include "io/reader_factory.h"

#include "tagger/tagdict.h"
#include "tagger/tagsetdict.h"
#include "tagger/tagger.h"
#include "tagger/super.h"

#include "parser/parser.h"
#include "parser/decoder_factory.h"

#include "parser/printer.h"
#include "parser/print_stream.h"
#include "parser/print_factory.h"

#include "parser/integration.h"

#include "timer.h"

#include "rnn_tagger.h"
#include "train_xf1.h"

const char *PROGRAM_NAME = "parser";

using namespace std;
using namespace NLP;
using namespace NLP::IO;
using namespace NLP::Taggers;
using namespace NLP::CCG;

int
run(int argc, char** argv){
  std::ostringstream PREFACE;
  PREFACE << start_preface(argc, argv);

  Config::Main cfg(PROGRAM_NAME);
  Super::Config super_cfg;
  Parser::Config parser_cfg;
  Integration::Config int_cfg("int");

  Config::Alias parser_alias(cfg, SPACE, parser_cfg, "model", "parser");
  Config::Alias super_alias(cfg, super_cfg, "super", "super");

  Config::Op<std::string> input_file(cfg, SPACE, "input", "the input file to read from", STDIN);
  Config::Op<std::string> input_file_dev(cfg, SPACE, "input_dev", "the input sec00 file to read from", STDIN);

  Config::Op<IO::Format> ifmt(cfg, "ifmt", "the input file format", Format("%w|%p \n"));
  Config::Op<IO::Format> ifmt_dev(cfg, "ifmt", "the input file format", Format("%w|%p|%s \n"));

  Config::Op<std::string> output_file(cfg, SPACE, "output", "the output file to write to", STDOUT);
  Config::Op<std::string> output_file_xf1(cfg, SPACE, "output_xf1", "the output file to write to", STDOUT);

  Config::Op<std::string> log_file(cfg, "log", "the log file to write to", STDERR);
  Config::Op<std::string> prefix(cfg, "prefix", "the prefix of the output and log files to write to", "");

  Config::Restricted<std::string> decoder_name(cfg, SPACE, "decoder", "the parser decoder [deps, derivs, random]",
					       &DecoderFactory::check, "derivs");
  Config::Restricted<std::string> printer_name(cfg, "printer", "the parser output printer [deps, prolog, boxer, ccgbank, grs, xml, debug, js]", &PrinterFactory::check, "grs");
  Config::Op<bool> force_words(cfg, "force_words", "force the parser to print words on fails", true);

  Config::Op<bool> oracle(cfg, SPACE, "oracle", "use gold standard supertags from input", false);
  Config::Op<bool> ext_super(cfg, "ext_super", "use an external supertags from input", false);

  Config::Alias start_alias(cfg, SPACE, int_cfg.start, "start_level", "int-start_level");
  Config::Alias betas_alias(cfg, int_cfg.betas, "betas", "int-betas");
  Config::Alias dict_cutoff_alias(cfg, int_cfg.dict_cutoffs, "dict_cutoffs", "int-dict_cutoffs");

  Config::Op<std::string> model_dir(cfg, "xf1_rnn_model_output_dir", "the output dir to put the trained tagger model", "./");
  Config::Op<bool> use_rnn_tagger(cfg, "use_rnn_tagger", "use rnn supertager", true);

  Config::Op<std::string> rnn_super_model(cfg, "rnn_super_model", "rnn super model", "./");
  Config::Op<std::string> emb_file(cfg, "emb", "file name of the embeddings file (*UNKONWN* changed to ***PADDING***, otherwise untouched)", "");
  Config::Op<std::string> suf_file(cfg, "suf", "file name of the suffix file", "");

  cfg.reg(int_cfg, SPACE);
  cfg.reg(parser_cfg, SPACE);
  cfg.reg(super_cfg, SPACE);

  cfg.parse(argc, argv);
  cfg.check();

  if(prefix() != ""){
    output_file.set_value(prefix() + ".out");
    log_file.set_value(prefix() + ".log");
  }

  if(printer_name() == "grs" && !parser_cfg.alt_markedup.has_changed())
    parser_cfg.alt_markedup.set_value(true);

  Sentence sent;
  Sentence sent1;

  ReaderFactory reader(input_file(), ifmt());

  Integration integration(int_cfg, super_cfg, parser_cfg, sent);

  StreamPrinter::Format FMT = StreamPrinter::FMT_DEV |
    StreamPrinter::FMT_PRINT_UNARY;

  if(force_words())
    FMT |= StreamPrinter::FMT_FORCE_WORDS;

  IO::Output out(output_file());
  IO::Log log(log_file());
  PrinterFactory printer(printer_name(), out, log, integration.cats, FMT);

  printer.header(PREFACE.str());

  DecoderFactory decoder(decoder_name());

  Stopwatch watch;

  ReaderFactory reader_dev(input_file_dev(), ifmt_dev());
  RnnTagger rnn_super(rnn_super_model(), emb_file(), suf_file(), 0.75, true);
  rnn_super.tag_file(reader_dev, true);
  //rnn_super.eval_mtag(reader_dev);


  //reader_dev.reset();

  bool USE_SUPER = not (oracle() or ext_super());
  if (ext_super()) {
    cerr << "parsing using easyccg supertags\n";
    integration.load_supercats("./easy_ccg_supercats");
  }
  int i = -1;
  while(reader.next(sent)){
    ++i;
    if(oracle())
      sent.copy_multi('s', 'S');

    if(sent.words.size() == 0){
      log.stream << "end of input" << endl;
      break;
    }

    if (ext_super()) {
      integration.parse_ext_super(sent, decoder, printer, i);
    }
    else if (use_rnn_tagger()) {
      rnn_super.mtag_sent(sent, 0.0);
      integration.parse_rnn(sent, decoder, printer);
    }
    else
      integration.parse(sent, decoder, printer);

  }

//  ReaderFactory reader_dev(input_file_dev(), ifmt());
//
//  XF1Trainer xf1_trainer(200, 23085, 426, 7, 3, 619, 9,
//                         50, 5, 5, 0.01, ignore_0_f1_seq(),
//                         wx_mat, wh_mat, wy_mat, emb_mat, suf_mat, cap_mat, 100,
//                         parser_cfg, 2, decoder, printer);
//  xf1_trainer.load_skip_ids("./skipped_ids");
//  xf1_trainer.init_training("./train_words_intersection.txt", "./train_labels_intersection.txt");
//  for (size_t e = 0; e < 50; ++e) {
//    cerr << "xf1 training" << " epoch: " << e << endl;
//    xf1_trainer.train(reader, FMT);
//    xf1_trainer.mtag(true);
//    reader_dev.reset();
//    out.fstream.close();
//    out.fstream.open(output_file().c_str(), std::fstream::out | std::fstream::trunc);
//
//    int i = -1;
//    while(reader_dev.next(sent1)){
//      ++i;
//      if(oracle())
//        sent1.copy_multi('s', 'S');
//
//      if(sent1.words.size() == 0){
//        log.stream << "end of input" << endl;
//        break;
//      }
//      integration1.parse_rnn(sent1, decoder, printer1, xf1_trainer.m_tagger.m_all_res_dev[i]);
//    }
//
//    xf1_trainer.save_model(model_dir.value, e);
//
//    out.fstream.close();
//    string newname = output_file_xf1.value + to_string(e);
//    std::ifstream  src(output_file().c_str(), std::ios::binary);
//    std::ofstream  dst(newname.c_str(),   std::ios::binary);
//    dst << src.rdbuf();
//    dst.close();
//    src.close();
//  }
//
//  return 0;

//  bool USE_SUPER = not (oracle() or ext_super());
//  int i = -1;
//  while(reader.next(sent)){
//    ++i;
//    if(oracle())
//      sent.copy_multi('s', 'S');
//
//    if(sent.words.size() == 0){
//      log.stream << "end of input" << endl;
//      break;
//    }
//
//    //integration.parse_rnn(sent, decoder, printer, rnn_tagger.m_all_res_dev[i]);
//  }
  printer.footer();

  double seconds = watch.stop();
  double sentence_speed = integration.nsentences / seconds;
  double word_speed = integration.nwords / seconds;

  log.stream << endl;

  log.stream << "use super = " << USE_SUPER << endl;
  log.stream << "beta levels = " << integration.BETAS << endl;
  log.stream << "dict cutoffs = " << integration.DICT_CUTOFFS << endl;
  log.stream << "start level = " << integration.START << endl;

  log.stream << "nwords = " << integration.nwords << endl;
  log.stream << "nsentences = " << integration.nsentences << endl;

  log.stream << "nexceptions = " << integration.nexceptions << endl;

  log.stream << "ncombines = " << integration.global_stats.ncombines << endl;
  log.stream << "ncombines_zeros = " << integration.global_stats.ncombines_zeros << endl;
  log.stream << "ncombines_reduced = " << integration.global_stats.ncombines_reduced << endl;
  log.stream << "ncombines_rejected = " << integration.global_stats.ncombines_rejected << endl;

  ulong nfail_bound = integration.nfail_nospan + integration.nfail_explode;
  ulong nfail_back = integration.nfail_nospan_explode + integration.nfail_explode_nospan;
  ulong nfail = nfail_bound + nfail_back;

  log.stream << "nfailures = " << nfail << endl;
  log.stream << "  run out of levels = " << nfail_bound << endl;
  log.stream << "    nospan = " << integration.nfail_nospan << endl;
  log.stream << "    explode = " << integration.nfail_explode << endl;
  log.stream << "  backtrack on levels = " << nfail_back << endl;
  log.stream << "    nospan/explode = " << integration.nfail_nospan_explode << endl;
  log.stream << "    explode/nospan = " << integration.nfail_explode_nospan << endl;

  for(int i = 0; i < static_cast<int>(integration.BETAS.size()); ++i){
    log.stream << "nsuccess " << i << ' ' << setw(6) << integration.BETAS[i];
    log.stream << ' ' << setw(6) << integration.nsuccesses[i];
    if(i == integration.START)
      log.stream << " <--";
    log.stream << endl;
  }

  log.stream << "total parsing time = " << seconds << " seconds\n";
  log.stream << "sentence speed = " << sentence_speed << " sentences/second\n";
  log.stream << "word speed = " << word_speed << " words/second\n";

  return 0;
}

#include "main.h"
