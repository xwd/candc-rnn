This repository contains code for the paper:  
  
[CCG Supertagging with a Recurrent Neural Network](https://aclanthology.info/pdf/P/P15/P15-2041.pdf)  
Wenduan Xu, Michael Auli, and Stephen Clark  
In Proc. ACL 2015

Instructions to follow...
