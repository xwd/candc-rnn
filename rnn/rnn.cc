// RNN Supertagger
// Copyright (c) Cambridge University Computer Lab
// Copyright (c) Wenduan Xu

#include <armadillo>
#include <sstream>
#include <string>
#include <unordered_map>
#include <stdexcept>
#include <random>
#include <cassert>

using namespace std;
using namespace arma;


mat *soft_max(mat y) {
  mat *res = new mat(exp(y)/accu(exp(y)));
  //cout << res.n_rows << " " << res.n_cols << endl;
  return res;
}


void x_ent_multi() {
  return;
}


pair<const mat *const, const mat *const>
calc_lh_proj_emb(int steps, int cs,
    const vector<vector<int> > &word_vecs,
    const mat **x_vals, mat &h_tm1,  mat &emb,
    mat &suffix, mat &cap, mat &wx,
    mat &wh, bool train) {
  colvec x;
  colvec x0;
  colvec temp;
  colvec x1;
  colvec temp1;
  colvec x2;
  colvec temp2;

  //cout << "word vecs size: " << word_vecs.size() << endl;

  for (int i = 0; i < cs; ++i) {
    //cout << "i: " << i << endl;
    //cout << "word vec i size: " << word_vecs[i].size() << endl;
    //printf("%d\n", word_vecs[i][0]);
    temp = emb.col(word_vecs[i][0]);
    x0 = join_cols(x0, temp);

    temp1 = suffix.col(word_vecs[i][1]);
    x1 = join_cols(x1, temp1);

    //printf("%d\n", word_vecs[i][2]);
    temp2 = cap.col(word_vecs[i][2]);
    x2 = join_cols(x2, temp2);
  }

  x = join_cols(join_cols(x0, x1), x2);

  const mat *const lh = soft_max(x.t()*wx + h_tm1*wh);
  mat one = ones<mat>(lh->n_rows, lh->n_cols);
  const mat *const lh_deriv = new mat(*lh%(one - *lh));

  pair<const mat *const, const mat *const> res(lh, lh_deriv);
  return res;
}


// bptt multi bs number of steps
void run_bptt_multi_emb(const vector<vector<vector<int> > > &word_vecs,
    mat &h_tm1, mat &emb, mat &suffix,
    mat &cap, mat &wx, mat &wy, mat &wh,
    double lr, const vector<int> &config) {
  int cs = config[0];
  int bs = config[1];
  int ds = config[2];
  int dc = config[3];
  int nh = config[4];
  int de = config[5];
  int steps = word_vecs.size();
  //cout << "steps: " << steps << endl;

  mat *lh_vals[steps + 1];
  lh_vals[0] = &h_tm1;
  const mat *lh_deriv_vals[steps];
  const mat *ly_vals[steps];
  const mat *err_vals[steps];
  const mat *x_vals[steps];

  const mat *delta_h_vals[steps];
  const mat *delta_x_vals[steps];


  for (int i = 0; i < steps; ++i) {
    //cout << "size: " << word_vecs[i].size() << endl;
    pair<const mat *const, const mat *const> vals =
        calc_lh_proj_emb(steps, cs, word_vecs[i], x_vals,
            h_tm1, emb, suffix, cap, wx, wh, true);
    const mat *const lh = vals.first;
    const mat *const lh_deriv = vals.second;
    ly_vals[i] = soft_max((*lh)*wy);
    lh_vals[i + 1] = new mat(lh->t());
    lh_deriv_vals[i] = lh_deriv;
  }


  for (int i = 0; i < steps; ++i) {


  }


  // bptt multi
  //  delta_h_vals[0] = &(*lh_vals[steps + 1] % (wy*ly_vals[steps]->t()));
  //  for (int i = steps, j = 0; i > 1; --i, ++j) {
  //     delta_h_vals[j + 1] = &(lh_deriv_vals[i - 2]->t() %
  //                           (wh*(*delta_h_vals[j]) + wy*(ly_vals[i - 2]->t())));
  //  }

  //  for (int i = 0, j = steps - 1; i < steps; ++i, --j) {
  //    wh = wh - lr%((*lh_vals[i])*delta_h_vals[j]->t());
  //  }
  //
  //  for (int i = 1; i <= steps; ++i) {
  //    wy = wy - lr%(lh_vals[i]*ly_vals[i - 1]);
  //  }
  //
  //  for (int i = 0, j = steps; j <= 1; ++i, --j) {
  //    delta_x_vals[i] = wx*delta_h_vals[j];
  //  }
  //
  //  for (int i = 0, j = steps; i < steps; ++i, --j) {
  //    wx = wx - lr%(x_vals[i]*delta_h_vals[j].t());
  //
  //   int start = 0;
  //   int start2 = de*cs;
  //   int start3 = start2 + ds*cs;
  //   for (int k = 0; k < cs; ++k) {
  //      emb.col(word_vecs[i][k][0]) = emb.col(word_vecs[i][k][0]) -
  //                                            lr*delta_x_vals[i].rows(start, start + de - 1);
  //      suffix.col(word_vecs[i][k][1]) = suffix.col(word_vecs[i][k][1]) -
  //                                                  lr*delta_x_vals[i].rows(start2, start2 + ds - 1);
  //      cap.col(word_vecs[i][k][2]) = cap.col(word_vecs[i][k][2]) -
  //                                            lr*delta_x_vals[i].rows(start3, start3 + dc -1);
  //      start += de;
  //      start2 += ds;
  //      start3 += dc;
  //    }
  //  }
}


vector<vector<vector<int> > > load_words(const string &filename,
    unordered_map<int, int> &voc,
    int &vocsize) {
  ifstream in(filename.c_str());
  if (!in)
    throw runtime_error("no such file: " + filename);
  string line;
  vector<vector<vector<int> > > data;
  //int line_count = 0;
  int i = 0;
  vector<vector<int> > sent;
  while (getline(in, line)) {
    //++line_count;
    //cout << line_count << endl;
    if (line.empty()) {
      data.push_back(sent);
      sent.clear();
      continue;
    }
    vector<int> word;
    int ind;
    i = 0;
    istringstream iss(line);
    //cout << "line: " << line << endl;
    while (iss >> ind) {
      word.push_back(ind);
      ++i;
      if (i == 1 && voc.insert(make_pair(ind, 0)).second)
        ++vocsize;
    }
    //cout << "word size: " << word.size() << endl;
    assert(word.size() == 3);
    sent.push_back(word);
  }
  //    for (vector<vector<int> >::iterator itr = sent.begin(); itr != sent.end(); ++itr) {
  //      for (vector<int>::iterator itr2 = itr->begin(); itr2 != itr->end(); ++itr2) {
  //        cout << *itr2 << " ";
  //      }
  //    }
  //    cout << endl;
  //cout << "here\n";
  return data;
}


vector<vector<int> > load_labels(const string &filename,
    unordered_map<int, int> &classes,
    int &nclasses) {
  ifstream in(filename.c_str());
  if (!in)
    throw runtime_error("no such file: " + filename);
  string line;
  vector<vector<int> > data;
  while (in) {
    if (!getline(in, line)) break;
    istringstream iss(line);
    vector<int> labels;
    int label;
    while (iss >> label) {
      if (classes.insert(make_pair(label, 0)).second) {
        ++nclasses;
      }
      labels.push_back(label);
    }
    data.push_back(labels);
  }

  return data;

  //  for (vector<vector<int> >::iterator itr = data.begin(); itr != data.end(); ++itr) {
  //    for (vector<int>::iterator itr2 = itr->begin(); itr2 != itr->end(); ++itr2) {
  //      cout << *itr2 << " ";
  //    }
  //    cout << endl;
  //  }
}


void load_emb_mat(mat &emb, const string &filename, int ne) {
  ifstream in(filename.c_str());
  if (!in)
    throw runtime_error("no such file: " + filename);
  string line;
  bool odd = true;
  int ind;
  while (in) {
    if (!getline(in, line)) break;
    istringstream iss(line);
    if (odd) {
      iss >> ind; 
      odd = false; 
    } else {
      mat temp(line);
      if (ind == -1) {
        emb.col(ne - 1) = temp.t();
      } else {
        emb.col(ind) = temp.t();
        odd = true;
      }
    }
  }
}


int load_suffix_count(const string &filename) {
  ifstream in(filename.c_str());
  if (!in)
    throw runtime_error("no such file: " + filename);
  int count;
  in >> count;
  return count;
}


void sent2contextwin(const vector<vector<int> > &sent,
    vector<vector<vector<int> > > &contextwins,
    int cs, int word_size, int unk_word_ind, int unk_suf_ind) {
  assert(cs % 2 == 1);
  int pad_len = cs/2;

  vector<int> unk_vec;
  unk_vec.push_back(unk_word_ind);
  unk_vec.push_back(unk_suf_ind);
  unk_vec.push_back(0);

  for (int i = 0; i < sent.size(); ++i) {
    vector<vector<int> > contextwin;
    for (int j = i, k =0; j < i + cs; ++j, ++k) {
      if (j < pad_len || j >= pad_len + sent.size()) {
        contextwin.push_back(unk_vec);
      } else {
        //cout << "sent[j - pad_len] size: " << sent[j - pad_len].size() << endl;
        contextwin.push_back(sent[j - pad_len]);
      }
    }
    contextwins.push_back(contextwin);
    //cout << "size cw: " << contextwins[i].size() << endl;
  }
}


void contextwin2minibatch(const vector<vector<vector<int> > > &sent_cw, 
    vector<vector<vector<vector<int> > > > &minibatch_all,
    int bs) {
  vector<vector<vector<int> > > minibatch;
  for (int i = 0; i < sent_cw.size(); ++i) {
    minibatch.push_back(sent_cw[i]);
    if ((i + 1) % bs == 0 || i == sent_cw.size() - 1) {
      minibatch_all.push_back(minibatch);
      minibatch.clear();
    }
  }
}


int main(int argc, char** argv) {
  bool verbose = false;
  double lr = 0.0627142536696559;
  int cs = 7; // number of words in the context window
  int bs = 9; // number of bptt time steps (i.e. mini-batch size)
  int ds = 5; // suffix emb dimensions
  int dc = 5; // cap emb dimensions
  int nh = 100; // hidden layer size
  int de = 50; // word emb dimensions
  int ws = 3; // word vec size (number of inds in each word vec)
  int nepochs = 50;

  std::mt19937 engine;
  std::uniform_real_distribution<double> uni(-1.0, 1.0);

  vector<int> config;
  config.push_back(cs);
  config.push_back(bs);
  config.push_back(ds);
  config.push_back(dc);
  config.push_back(nh);
  config.push_back(de);

  // load data

  unordered_map<int, int> voc;
  unordered_map<int, int> classes;
  int vocsize = 0;
  int nclasses = 0;

  vector<vector<vector<int> > > train_words;
  vector<vector<int> > train_labels;
  vector<vector<vector<int> > > dev_words;
  vector<vector<int> > dev_labels;
  vector<vector<vector<int> > > test_words;
  vector<vector<int> > test_labels;

  try {
    train_words = load_words("./train_words.txt", voc, vocsize);
    train_labels = load_labels("./train_labels.txt", classes, nclasses);
    dev_words = load_words("./dev_words.txt", voc, vocsize);
    dev_labels = load_labels("./dev_labels.txt", classes, nclasses);
    test_words = load_words("./test_words.txt", voc, vocsize);
    test_labels = load_labels("./test_labels.txt", classes, nclasses);
  } catch (runtime_error &err) {
    cerr << err.what() << ", exit now." << endl;
    return -1;
  }


  // init all embedding matrices

  mat emb(de, vocsize + 4);
  emb.imbue( [&]() { return uni(engine); } );
  emb = emb * 0.2;
  load_emb_mat(emb, "./reduced_emb2idx_dict", vocsize+4);

  int suffix_count = load_suffix_count("./suffix_count.txt");
  mat suffix(ds, suffix_count + 1);
  suffix.imbue( [&]() { return uni(engine); } );
  suffix = suffix * 0.2;

  mat cap(dc, 2);
  cap.imbue( [&]() { return uni(engine); } );
  cap = cap * 0.2;

  mat wx((de + ds + dc)*cs, nh);
  wx.imbue( [&]() { return uni(engine); } );
  wx = wx * 0.2;

  mat wy(nh, nclasses);
  wy.imbue( [&]() { return uni(engine); } );
  wy = wy * 0.2;

  mat wh(nh, nh);
  wh.imbue( [&]() { return uni(engine); } );
  wh = wh * 0.2;

  mat h_tm1(1, nh);
  h_tm1.zeros();

  printf ("vocsize: %d, nclasses: %d, total 2char suffix: %d\n",
      vocsize, nclasses, suffix_count);

  // test
  //  vector<vector<int> > temp;
  //  vector<int> word;
  //  word.push_back(1);
  //  word.push_back(2);
  //  word.push_back(0);
  //
  //  for (int i = 0; i < 3; ++i) {
  //    temp.push_back(word);
  //  }
  //
  //  vector<vector<vector<int> > > contextwins;
  //  sent2contextwin(temp, contextwins, cs, ws, vocsize + 4 - 1, suffix_count);
  //  for (int i = 0; i < contextwins.size(); ++i) {
  //    for (int j = 0; j < contextwins[i].size(); ++j) {
  //      for (int k = 0; k < contextwins[i][j].size(); ++k) {
  //        cout << contextwins[i][j][k] << ", ";
  //      }
  //    }
  //    cout << endl;
  //  }
  //
  //
  //  vector<vector<vector<vector<int> > > > minibatch_sent_all;
  //  contextwin2minibatch(contextwins, minibatch_sent_all, bs);
  //  for (int a = 0; a < minibatch_sent_all.size(); ++a) {
  //    for (int b = 0; b < minibatch_sent_all[a].size(); ++b) {
  //      for (int c = 0; c < minibatch_sent_all[a][b].size(); ++c) {
  //        for (int d = 0; d < minibatch_sent_all[a][b][c].size(); ++d) {
  //          cout << minibatch_sent_all[a][b][c][d] << ", ";
  //        }
  //      }
  //      cout << endl;
  //    }
  //  }
  //
  //  for (int k = 0; k < minibatch_sent_all.size(); ++k) {
  //    run_bptt_multi_emb(minibatch_sent_all[k], h_tm1, emb, suffix,
  //        cap, wx, wy, wh, lr, config);
  //  }

  // test
  //      for (int a = 0; a < minibatch_sent_all.size(); ++a) {
  //        for (int b = 0; b < minibatch_sent_all[a].size(); ++b) {
  //          for (int c = 0; c < minibatch_sent_all[a][b].size(); ++c) {
  //            for (int d = 0; d < minibatch_sent_all[a][b][c].size(); ++d) {
  //              cout << minibatch_sent_all[a][b][c][d] << ", ";
  //            }
  //          }
  //        }
  //        cout << endl;
  //      }

  //cout << contextwins.size() << endl;
//      for (int i = 0; i < contextwins.size(); ++i) {
//        for (int j = 0; j < contextwins[i].size(); ++j) {
//          for (int k = 0; k < contextwins[i][j].size(); ++k) {
//            cout << contextwins[i][j][k] << ", ";
//          }
//        }
//      }

  vector<vector<vector<int> > > contextwins;
  vector<vector<vector<vector<int> > > > minibatch_sent_all;
  printf ("training started...\n");
  for (int i = 0; i < nepochs; ++i) {
    printf ("epoch %d", i);
    for (int j = 0; j < train_words.size(); ++j) {
      //cout << j << endl;
      contextwins.clear();
      minibatch_sent_all.clear();
      sent2contextwin(train_words[j], contextwins, cs, ws, vocsize + 4 - 1, suffix_count);
      contextwin2minibatch(contextwins, minibatch_sent_all, bs);
      for (int k = 0; k < minibatch_sent_all.size(); ++k) {
        run_bptt_multi_emb(minibatch_sent_all[k], h_tm1, emb, suffix,
            cap, wx, wy, wh, lr, config);
      }
    }
    printf (" finished \n");
  }

  return 0;
}
