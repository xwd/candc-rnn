// Copyright (c) Cambridge University Computer Lab
// Copyright (c) Wenduan Xu

#include <boost/program_options.hpp>
#include <sstream>
#include <string>
#include <unordered_map>
#include <stdexcept>
#include <random>
#include <cassert>
#include <armadillo>
#include <time.h>
#include <limits>
#include <cstdlib>

namespace po = boost::program_options;
using namespace std;
using namespace arma;


mat sigmoid(const mat &m) {
  mat one = ones<mat>(m.n_rows, m.n_cols);
  return one/(one + exp(-m));
}


mat soft_max(const mat &y) {
  mat res = exp(y)/accu(exp(y));
  return res;
}


double x_ent_multi(const mat&y, const mat&t) {
  return -accu(t % log(y));
}


void sign_w(mat &m) {
  for (mat::iterator i = m.begin(); i != m.end(); ++i) {
    if (*i > 0) *i = 1.0;
    if (*i < 0) *i = -1.0;
    if (*i == 0) *i = 0.0;
  }
}


pair<mat, mat> calc_lh_proj_emb(int step, int steps, int cs,
                                const vector<vector<int> > &word_vecs,
                                mat *x_vals, mat &h_tm1,  mat &emb,
                                mat &suffix, mat &cap, mat &wx,
                                mat &wh, bool train) {
  colvec x;
  colvec x0;
  colvec x1;
  colvec x2;

  for (int i = 0; i < cs; ++i) {
    x0 = join_cols(x0, emb.col(word_vecs[i][0]));
    x1 = join_cols(x1, suffix.col(word_vecs[i][1]));
    x2 = join_cols(x2, cap.col(word_vecs[i][2]));
  }

  x = join_cols(join_cols(x0, x1), x2);
  x_vals[step] = x;

  mat lh = sigmoid(x.t()*wx + h_tm1*wh);
  mat one = ones<mat>(lh.n_rows, lh.n_cols);
  mat lh_deriv = lh%(one - lh);

  pair<mat, mat> res(lh, lh_deriv);
  return res;
}


// bptt multi bs number of steps, each step corresponds to a contextwin
double train_bptt_multi_emb(const vector<vector<vector<int> > > &contextwins,
                            const vector<int> &label_batch,
                            mat &h_tm1, mat &emb, mat &suffix,
                            mat &cap, mat &wx, mat &wy, mat &wh,
                            double lr, const vector<int> &config, mat &y) {
  int cs = config[0];
  int bs = config[1];
  int ds = config[2];
  int dc = config[3];
  int nh = config[4];
  int de = config[5];
  int steps = contextwins.size();

  double err = 0.0;

  mat lh_vals[steps + 1];
  lh_vals[0] = h_tm1;
  mat lh_deriv_vals[steps];
  mat ly_vals[steps];
  mat x_vals[steps];

  mat delta_h_vals[steps];
  mat delta_x_vals[steps]; 

  for (int i = 0; i < steps; ++i) {
    pair<mat,  mat> vals =
        calc_lh_proj_emb(i, steps, cs, contextwins[i], x_vals,
                         lh_vals[i], emb, suffix, cap, wx, wh, true);
    mat lh = vals.first;
    mat lh_deriv = vals.second;
    ly_vals[i] = soft_max(lh*wy);
    lh_vals[i + 1] = lh;
    lh_deriv_vals[i] = lh_deriv;
  }

  for (int i = 0; i < steps; ++i) {
    y[label_batch[i]] = 1.0;
    err += x_ent_multi(ly_vals[i], y);
    ly_vals[i](label_batch[i]) -= 1.0;
    y[label_batch[i]] = 0.0;
  }

  // bptt multi
  delta_h_vals[steps - 1] = lh_deriv_vals[steps - 1].t()
                                          % (wy*ly_vals[steps - 1].t());
  for (int i = steps; i > 1; --i) {
    delta_h_vals[i - 2] = lh_deriv_vals[i - 2].t() %
        ((wh*delta_h_vals[i - 1]) + wy*(ly_vals[i - 2].t()));
  }

  for (int i = 0; i < steps; ++i) {
    mat grad = lh_vals[i].t()*delta_h_vals[i].t();
    wh -= lr*grad;
  }

  for (int i = 1; i <= steps; ++i) {
    mat grad = lh_vals[i].t()*ly_vals[i - 1];
    wy -= lr*grad;
  }

  for (int i = 0; i < steps; ++i) {
    delta_x_vals[i] = wx*delta_h_vals[i];
  }

  for (int i = 0; i < steps; ++i) {
    mat grad = x_vals[i]*delta_h_vals[i].t();
    wx -= lr*grad;
  }

  for (int i = 0; i < steps; ++i) {
    int start = 0;
    int start2 = de*cs;
    int start3 = start2 + ds*cs;
    for (int k = 0; k < cs; ++k) {
      emb.col(contextwins[i][k][0]) -=
          lr*delta_x_vals[i].rows(start, start + de - 1);
      suffix.col(contextwins[i][k][1]) -=
          lr*delta_x_vals[i].rows(start2, start2 + ds - 1);
      cap.col(contextwins[i][k][2]) -=
          lr*delta_x_vals[i].rows(start3, start3 + dc - 1);
      start += de;
      start2 += ds;
      start3 += dc;
    }
  }

  h_tm1 = lh_vals[steps];
  return err;
}


// bptt multi bs number of steps, each step corresponds to a contextwin
double train_bptt_multi_emb_reg(const vector<vector<vector<int> > > &word_vecs,
                            const vector<int> &label_batch,
                            mat &h_tm1, mat &emb, mat &suffix,
                            mat &cap, mat &wx, mat &wy, mat &wh,
                            double lr, const vector<int> &config, mat &y,
                            mat &ly_freq, mat &emb_freq, mat &suf_freq, mat &cap_freq,
                            bool regl1, bool regl2,
                            bool reg_freq, double BoverT, double lambda) {
  int cs = config[0];
  int bs = config[1];
  int ds = config[2];
  int dc = config[3];
  int nh = config[4];
  int de = config[5];
  int steps = word_vecs.size();

  double err = 0.0;
  uword y_ind;

  mat lh_vals[steps + 1];
  lh_vals[0] = h_tm1;
  mat lh_deriv_vals[steps];
  mat ly_vals[steps];
  mat x_vals[steps];

  mat delta_h_vals[steps];
  mat delta_x_vals[steps]; 

  mat reg_wy;
  mat reg_emb;
  mat reg_suf;
  mat reg_cap;

  double bt = 1.0;
  if (BoverT != 0.0)
    bt = BoverT;

  for (int i = 0; i < steps; ++i) {
    pair<mat,  mat> vals =
        calc_lh_proj_emb(i, steps, cs, word_vecs[i], x_vals,
                         lh_vals[i], emb, suffix, cap, wx, wh, true);
    mat lh = vals.first;
    mat lh_deriv = vals.second;
    ly_vals[i] = soft_max(lh*wy);
    if (reg_freq) {
      ly_vals[i].max(y_ind);
      ly_freq(y_ind) += 1.0;
    }
    lh_vals[i + 1] = lh;
    lh_deriv_vals[i] = lh_deriv;
  }

  for (int i = 0; i < steps; ++i) {
    y[label_batch[i]] = 1.0;
    err += x_ent_multi(ly_vals[i], y);
    ly_vals[i](label_batch[i]) -= 1.0;
    y[label_batch[i]] = 0.0;
  }

  // bptt multi
  delta_h_vals[steps - 1] = lh_deriv_vals[steps - 1].t()
                                          % (wy*ly_vals[steps - 1].t());
  for (int i = steps; i > 1; --i) {
    delta_h_vals[i - 2] = lh_deriv_vals[i - 2].t() %
        ((wh*delta_h_vals[i - 1]) + wy*(ly_vals[i - 2].t()));
  }

  for (int i = 0; i < steps; ++i) {
    mat grad = lh_vals[i].t()*delta_h_vals[i].t();
    if (regl1) {
      mat temp = wh;
      sign_w(temp);
      grad += lambda*bt*temp;
    }
    if (regl2)
      grad += 2*lambda*bt*wh;
    wh -= lr*grad;
  }

  for (int i = 1; i <= steps; ++i) {
    mat grad = lh_vals[i].t()*ly_vals[i - 1];

    if (regl2 || regl1) {
      if (regl2 && !regl1)
        reg_wy = 2*lambda*bt*wy;
      if (regl1 && !regl2) {
         mat temp = wy;
         sign_w(wy);
         reg_wy = lambda*bt*temp;
      }
      if (regl1 && regl2) {
        mat temp = wy;
        sign_w(wy);
        reg_wy = bt*lambda*(2*wy + temp);
      }
      if (reg_freq) {
        for (uword c = 0; c < ly_freq.n_cols; ++c) {
          if (ly_freq(c) != 0.0)
            reg_wy.col(c) *= 1.0/ly_freq(c);
        }
      }
      wy -= lr*(grad + reg_wy);
    } else {
      wy -= lr*grad;
    }
  }

  for (int i = 0; i < steps; ++i) {
    delta_x_vals[i] = wx*delta_h_vals[i];
  }

  for (int i = 0; i < steps; ++i) {
    mat grad = x_vals[i]*delta_h_vals[i].t();
    if (regl2) {
      grad += 2*lambda*bt*wx;
    }
    if (regl1) {
      mat temp = wx;
      sign_w(temp);
      grad += lambda*bt*temp;
    }
    wx -= lr*grad;
  }

  for (int i = 0; i < steps; ++i) {
    int start = 0;
    int start2 = de*cs;
    int start3 = start2 + ds*cs;
    for (int k = 0; k < cs; ++k) {
      int emb_ind = word_vecs[i][k][0];
      int suf_ind = word_vecs[i][k][1];
      int cap_ind = word_vecs[i][k][2];

      mat grad_emb = delta_x_vals[i].rows(start, start + de - 1);
      mat grad_suf = delta_x_vals[i].rows(start2, start2 + ds - 1);
      mat grad_cap = delta_x_vals[i].rows(start3, start3 + dc - 1);

      // reg freq counter
      if (regl2 || regl1) {
        if (regl2) {
          reg_emb = 2*lambda*bt*emb.col(emb_ind);
          reg_suf = 2*lambda*bt*suffix.col(suf_ind);
          reg_cap = 2*lambda*bt*cap.col(cap_ind);
        }

        if (regl1 && !regl2) {
          mat temp1 = emb.col(emb_ind);
          mat temp2 = suffix.col(suf_ind);
          mat temp3 = cap.col(cap_ind);
          sign_w(temp1);
          sign_w(temp2);
          sign_w(temp3);
          reg_emb = lambda*bt*temp1;
          reg_suf = lambda*bt*temp2;
          reg_cap = lambda*bt*temp3;
        }

        if (regl1 && regl2) {
          mat temp1 = emb.col(emb_ind);
          mat temp2 = suffix.col(suf_ind);
          mat temp3 = cap.col(cap_ind);
          sign_w(temp1);
          sign_w(temp2);
          sign_w(temp3);
          reg_emb += lambda*bt*temp1;
          reg_suf += lambda*bt*temp2;
          reg_cap += lambda*bt*temp3;
        }

        if (reg_freq) {
          emb_freq.col(emb_ind) += 1.0;
          suf_freq.col(suf_ind) += 1.0;
          cap_freq.col(cap_ind) += 1.0;

          if (emb_freq(emb_ind) != 0.0)
            reg_emb *= 1.0/emb_freq(emb_ind);
          if (suf_freq(suf_ind) != 0.0)
            reg_suf *= 1.0/suf_freq(suf_ind);
          if (cap_freq(cap_ind) != 0.0)
            reg_cap *= 1.0/cap_freq(cap_ind);
        }
        emb.col(emb_ind) -= lr*(grad_emb + reg_emb);
        suffix.col(suf_ind) -= lr*(grad_suf + reg_suf);
        cap.col(cap_ind) -= lr*(grad_cap + reg_cap);

      } else {
        emb.col(emb_ind) -= lr*grad_emb;
        suffix.col(suf_ind) -= lr*grad_suf;
        cap.col(cap_ind) -= lr*grad_cap;
      }

      start += de;
      start2 += ds;
      start3 += dc;
    }
  }

  h_tm1 = lh_vals[steps];
  return err;
}


vector<vector<vector<int> > >
load_words(const string &filename,
           unordered_map<int, int> &voc,
           int &vocsize) {
  ifstream in(filename.c_str());
  if (!in)
    throw runtime_error("no such file: " + filename);
  string line;
  vector<vector<vector<int> > > data;
  int i = 0;
  vector<vector<int> > sent;
  while (getline(in, line)) {
    if (line.empty()) {
      data.push_back(sent);
      sent.clear();
      continue;
    }
    vector<int> word;
    int ind;
    i = 0;
    istringstream iss(line);
    while (iss >> ind) {
      word.push_back(ind);
      ++i;
      if (i == 1 && voc.insert(make_pair(ind, 0)).second)
        ++vocsize;
    }
    assert(word.size() == 3);
    sent.push_back(word);
  }
  return data;
}


vector<vector<int> >
load_labels(const string &filename,
            unordered_map<int, int> &classes,
            int &nclasses, int &ntokens) {
  ifstream in(filename.c_str());
  if (!in)
    throw runtime_error("no such file: " + filename);
  string line;
  vector<vector<int> > data;
  while (in) {
    if (!getline(in, line)) break;
    istringstream iss(line);
    vector<int> labels;
    int label;
    while (iss >> label) {
      if (classes.insert(make_pair(label, 0)).second) {
        ++nclasses;
      }
      labels.push_back(label);
    }
    ntokens += labels.size();
    data.push_back(labels);
  }
  return data;
}


void load_emb_mat(mat &emb, const string &filename, int ne) {
  ifstream in(filename.c_str());
  if (!in)
    throw runtime_error("no such file: " + filename);
  string line;
  bool odd = true;
  int ind;
  while (in) {
    if (!getline(in, line)) break;
    istringstream iss(line);
    if (odd) {
      iss >> ind;
      odd = false;
    } else {
      mat temp(line);
      if (ind == -1) {
        emb.col(ne - 1) = temp.t();
      } else {
        emb.col(ind) = temp.t();
        odd = true;
      }
    }
  }
}


int load_suffix_count(const string &filename) {
  ifstream in(filename.c_str());
  if (!in)
    throw runtime_error("no such file: " + filename);
  int count;
  in >> count;
  return count;
}


void sent2contextwin(const vector<vector<int> > &sent,
                     vector<vector<vector<int> > > &contextwins,
                     int cs, int word_size, int unk_word_ind, int unk_suf_ind) {
  assert(cs % 2 == 1);
  int pad_len = cs/2;
  vector<int> unk_vec;
  unk_vec.push_back(unk_word_ind);
  unk_vec.push_back(unk_suf_ind);
  unk_vec.push_back(0);

  for (int i = 0; i < sent.size(); ++i) {
    vector<vector<int> > contextwin;
    for (int j = i, k =0; j < i + cs; ++j, ++k) {
      if (j < pad_len || j >= pad_len + sent.size()) {
        contextwin.push_back(unk_vec);
      } else {
        contextwin.push_back(sent[j - pad_len]);
      }
    }
    contextwins.push_back(contextwin);
  }
}


void contextwin2minibatch(const vector<vector<vector<int> > > &sent_cw,
                          vector<vector<vector<vector<int> > > > &minibatch_all,
                          int bs) {
  vector<vector<vector<int> > > minibatch;
  for (int i = 0; i < sent_cw.size(); ++i) {
    minibatch.push_back(sent_cw[i]);
    if ((i + 1) % bs == 0 || i == sent_cw.size() - 1) {
      minibatch_all.push_back(minibatch);
      minibatch.clear();
    }
  }
}


void labels2minibatch(const vector<int> &labels, 
                      vector<vector<int> > &minibatch_all, int bs) {
  vector<int> minibatch;
  for (int i = 0; i < labels.size(); ++i) {
    minibatch.push_back(labels[i]);
    if ((i + 1) % bs == 0 || i == labels.size() - 1) {
      minibatch_all.push_back(minibatch);
      minibatch.clear();
    }
  }
}


void classify_emb(const vector<vector<vector<int> > > &contextwins,
                  const vector<int> &gold_labels,
                  mat &h_tm1, const mat &wx, const mat &wh,
                  const mat &wy, const mat &suffix, const mat &cap,
                  const mat &emb, int cs, vector<int> &sent_res,
                  mat &y, double &err) {
  h_tm1.zeros();
  uword ind;
  for (int i = 0; i < contextwins.size(); ++i) {
    colvec x;
    colvec x0;
    colvec temp;
    colvec x1;
    colvec temp1;
    colvec x2;
    colvec temp2;

    for (int j = 0; j < cs; ++j) {
      temp = emb.col(contextwins[i][j][0]);
      x0 = join_cols(x0, temp);

      temp1 = suffix.col(contextwins[i][j][1]);
      x1 = join_cols(x1, temp1);

      temp2 = cap.col(contextwins[i][j][2]);
      x2 = join_cols(x2, temp2);
    }

    x = join_cols(join_cols(x0, x1), x2);
    mat lh = sigmoid(x.t()*wx + h_tm1*wh);
    mat ly = soft_max(lh*wy);
    y(gold_labels[i]) = 1.0;
    err += x_ent_multi(ly, y);
    y(gold_labels[i]) = 0.0;
    ly.max(ind);
    sent_res.push_back(ind);
    h_tm1 = lh;
  }
}


double eval_super(const vector<vector<vector<int> > > &gold_words,
                  const vector<vector<int> > &gold_labels, bool dev,
                  mat &h_tm1, const mat &wx, const mat &wh,
                  const mat &wy, const mat &suffix, const mat&cap,
                  const mat &emb, int cs, int ws, int vocsize, int suffix_count,
                  mat &y, int ntokens) {
  double err = 0.0;
  vector<int> sent_res;
  vector<vector<int> > all_res;
  vector<vector<vector<int> > > contextwins;
  for (int i = 0; i < gold_words.size(); ++i) {
    contextwins.clear();
    sent_res.clear();
    sent2contextwin(gold_words[i], contextwins, cs, ws, vocsize + 4 - 1, suffix_count);
    classify_emb(contextwins, gold_labels[i],
                 h_tm1, wx, wh, wy, suffix, cap, emb, cs, sent_res, y, err);
    all_res.push_back(sent_res);
  }

  double total = 0.0;
  double correct = 0.0;
  for (int k = 0; k < gold_labels.size(); ++k) {
    total += gold_labels[k].size();
    for (int l = 0; l < gold_labels[k].size(); ++l) {
      assert(gold_labels[k].size() == all_res[k].size());
      if (gold_labels[k][l] == all_res[k][l])
        ++correct;
    }
  }

  assert(correct <= total);
  if (dev) printf ("-- 00 accuracy: %f err: %f \n",
                    correct/total, err/(double)ntokens);
  else printf ("-- 23 accuracy: %f err: %f \n",
                correct/total, err/(double)ntokens);
  return err;
}


int main(int argc, char** argv) {
  po::options_description desc("Usage");
  double lr = 0.0;
  int cs = 0;
  int bs = 0;
  int ds = 0;
  int dc = 0;
  int nh = 0;
  int de = 0;
  int ws = 0;
  int nepoch = 0;
  int ess = 0;

  bool verbose = false;
  int decay = 0;
  bool early_stop = false;
  bool eval23 = false;

  bool regl1 = false;
  bool regl2 = false;
  bool reg_freq = false;
  bool reg_bt = false;
  double lambda  = 0.0;

  string dir;

  desc.add_options()
    ("help,h", "display this help message")
    ("cs", po::value<int>(&cs)->default_value(7), "number of words in a context window")
    ("bs", po::value<int>(&bs)->default_value(9), "number of bptt time steps (i.e. mini-batch size)")
    ("ds", po::value<int>(&ds)->default_value(5), "dimension of suffix embeddings")
    ("dc", po::value<int>(&dc)->default_value(5), "dimension of cap embeddings")
    ("de", po::value<int>(&de)->default_value(50), "dimension of word embeddings")
    ("nh", po::value<int>(&nh)->default_value(100), "hidden layer size")
    ("ne", po::value<int>(&nepoch)->default_value(50), "number of training epochs")
    ("ws", po::value<int>(&ws)->default_value(3), "number of indexes in a word vec")
    ("ess", po::value<int>(&ess)->default_value(100), "number of sentences for each early-stopping evaluation")
    ("lr", po::value<double>(&lr)->default_value(0.0025), "learning rate")
    ("lambda", po::value<double>(&lambda)->default_value(0.000001), "lambda: regularization penalty")
    ("v", po::value<bool>(&verbose)->default_value(false), "verbose output")
    ("decay", po::value<int>(&decay)->default_value(0), "learning rate decay")
    ("es", po::value<bool>(&early_stop)->default_value(false), "early stopping of training")
    ("23", po::value<bool>(&eval23)->default_value(true), "eval sec23")
    ("l1", po::value<bool>(&regl1)->default_value(false), "l1 regularization")
    ("l2", po::value<bool>(&regl2)->default_value(false), "l2 regularization")
    ("reg_freq", po::value<bool>(&reg_freq)->default_value(false), "frequency scaling for regularizations")
    ("reg_bt", po::value<bool>(&reg_bt)->default_value(false), "minibatch scaling for regularizations")
	("model", po::value<string>(&dir)->default_value("./model"), "model output directory");

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  system(("mkdir " + dir).c_str());

  if(vm.count("help")){
    cout << "\n" <<  desc << "\n";
    return 1;
  }

  printf ("param: lr: %.15f, cs: %d, bs: %d, ds: %d, dc: %d, nh: %d, de: %d, ws: %d\n",
           lr, cs, bs, ds, dc, nh, de, ws);

  std::mt19937 engine;
  std::uniform_real_distribution<double> uni(-1.0, 1.0);

  vector<int> config;
  config.push_back(cs);
  config.push_back(bs);
  config.push_back(ds);
  config.push_back(dc);
  config.push_back(nh);
  config.push_back(de);

  // load data

  unordered_map<int, int> voc;
  unordered_map<int, int> classes;
  int vocsize = 0;
  int nclasses = 0;
  int ntokens0221 = 0;
  int ntokens00 = 0;
  int ntokens23 = 0;

  vector<vector<vector<int> > > train_words;
  vector<vector<int> > train_labels;
  vector<vector<vector<int> > > dev_words;
  vector<vector<int> > dev_labels;
  vector<vector<vector<int> > > test_words;
  vector<vector<int> > test_labels;

  try {
    train_words = load_words("./train_words.txt", voc, vocsize);
    train_labels = load_labels("./train_labels.txt", classes, nclasses, ntokens0221);
    dev_words = load_words("./dev_words.txt", voc, vocsize);
    dev_labels = load_labels("./dev_labels.txt", classes, nclasses, ntokens00);
    test_words = load_words("./test_words.txt", voc, vocsize);
    test_labels = load_labels("./test_labels.txt", classes, nclasses, ntokens23);
  } catch (runtime_error &err) {
    cerr << err.what() << ", exit now." << endl;
    return -1;
  }

  assert(train_words.size() == train_labels.size());
  assert(dev_words.size() == dev_labels.size());
  assert(test_words.size() == test_labels.size());

  // init all embedding matrices

  mat emb(de, vocsize + 4);
  emb.imbue( [&]() { return uni(engine); } );
  emb = emb * 0.2;
  load_emb_mat(emb, "./reduced_emb2idx_dict", vocsize + 4);

  int suffix_count = load_suffix_count("./suffix_count.txt");
  mat suffix(ds, suffix_count + 1);
  suffix.imbue( [&]() { return uni(engine); } );
  suffix = suffix * 0.2;

  mat cap(dc, 2);
  cap.imbue( [&]() { return uni(engine); } );
  cap *= 0.2;

  mat wx((de + ds + dc)*cs, nh);
  wx.imbue( [&]() { return uni(engine); } );
  wx *= 0.2;

  mat wy(nh, nclasses);
  wy.imbue( [&]() { return uni(engine); } );
  wy *= 0.2;

  mat wh(nh, nh);
  wh.imbue( [&]() { return uni(engine); } );
  wh *= 0.2;

  mat h_tm1(1, nh);
  h_tm1.zeros();

  // regularization scaling matrices
  mat ly_freq(1, nclasses, fill::zeros);
  mat emb_freq(1, vocsize + 4, fill::zeros);
  mat suf_freq(1, suffix_count + 1, fill::zeros);
  mat cap_freq(1, 2, fill::zeros);

  // regularization B/T scaling
  double BoverT = 0.0;
  if (reg_bt) {
    int nminibatches = ntokens0221/bs;
    BoverT = 1.0/(double)nminibatches;
  }

  printf ("vocsize: %d, nclasses: %d, total 2char suffix: %d, nsentences: %d\n",
           vocsize, nclasses, suffix_count, (int)train_words.size());

  printf ("ntokens0221: %d, ntokens00: %d, ntokens23: %d \n",
          ntokens0221, ntokens00, ntokens23);

//  test
//  mat z(1, 3);
//  z << 0.4 << 0.3 << 0.3 << endr;
//  mat t(1, 3);
//  t << 1.0 << 0.0 << 0.0 << endr;
//  cout.precision(15);
//  cout << x_ent_multi(z, t) << endl;

  vector<int> train_data_inds;
  for (int i = 0; i < train_words.size(); ++i) {
    train_data_inds.push_back(i);
  }

  vector<vector<vector<int> > > contextwins;
  vector<vector<vector<vector<int> > > > minibatch_contextwins;
  vector<vector<int> > minibatch_labels;
  mat y = mat(1, nclasses, fill::zeros);

  time_t tstart;
  time_t tend;
  double secs = 0.0;

  printf ("training started...\n");
  double total_err = 0.0;
  double dev_err = numeric_limits<double>::max();
  double dev_err_new = 0.0;

  bool lr_decay = false;
  double lr_decay_epoch = 0.0;

  int current_word_ind = 0;
  for (int i = 0; i < nepoch; ++i) {
    if (lr_decay && decay == 1) {
      lr = 0.5*lr;
      printf (">> new decayed lr: %.15f\n", lr);
    }

    if (lr_decay && decay == 2) {
      lr = (lr_decay_epoch/(double)i)*lr;
      printf (">> new decayed lr: %.15f\n", lr);
    }

    time(&tstart);
    shuffle(begin(train_data_inds), std::end(train_data_inds), engine);
    total_err = 0.0;
    int j = 0;
    for (int s = 0; s < train_data_inds.size(); ++s) {
      j = train_data_inds[s];
      assert(train_words[j].size() == train_labels[j].size());

      h_tm1.zeros() ;
      contextwins.clear();
      minibatch_contextwins.clear();
      minibatch_labels.clear();
      sent2contextwin(train_words[j], contextwins, cs, ws, vocsize + 4 - 1, suffix_count);
      contextwin2minibatch(contextwins, minibatch_contextwins, bs);
      labels2minibatch(train_labels[j], minibatch_labels, bs);

      for (int k = 0; k < minibatch_contextwins.size(); ++k) {
        assert(minibatch_contextwins[k].size() == minibatch_labels[k].size());

        if (minibatch_contextwins[k].size() < bs) {
          BoverT = ((double)minibatch_contextwins[k].size()/bs)*BoverT;
        }

        if (regl1 || regl2)
          total_err += train_bptt_multi_emb_reg(minibatch_contextwins[k],
                   minibatch_labels[k],
                   h_tm1, emb, suffix,
                   cap, wx, wy, wh, lr, config, y,
                   ly_freq, emb_freq, suf_freq, cap_freq,
                   regl1, regl2, reg_freq, BoverT, lambda);
        else
          total_err += train_bptt_multi_emb(minibatch_contextwins[k],
            minibatch_labels[k],
            h_tm1, emb, suffix,
            cap, wx, wy, wh, lr, config, y);
      }
      time(&tend);
      if (verbose)
        printf (">> epoch %d, %f completed in %f secs <<\r", i,
          (double(s + 1)/(double)train_words.size())*100, difftime(tend, tstart));

      if (early_stop && s % ess == 0) {
        printf ("-- sentence %d, 0221 err: %.15f\n", s, total_err/(double)ntokens0221);
        eval_super(dev_words, dev_labels, true, h_tm1, wx, wh,
                wy, suffix, cap, emb, cs, ws, vocsize, suffix_count, y, ntokens00);
      }
    }

    time(&tend);
    secs = difftime(tend, tstart);
    printf ("\n>> time 0221: %f secs\n", secs);
    printf (">> epoch %d, err 0221: %f\n", i, total_err/(double)ntokens0221);

    time(&tstart);
    dev_err_new = eval_super(dev_words, dev_labels, true, h_tm1, wx, wh,
        wy, suffix, cap, emb, cs, ws, vocsize, suffix_count, y, ntokens00);
    time(&tend);
    printf (">> 00 eval time %f\n", difftime(tend, tstart));

    // save model
    wx.save(dir + "/wx." + to_string(i) + ".mat");
    wh.save(dir + "/wh." + to_string(i) + ".mat");
    wy.save(dir + "/wy." + to_string(i) + ".mat");
    suffix.save(dir + "/suffix." + to_string(i) + ".mat");
    cap.save(dir + "/cap." + to_string(i) + ".mat");
    emb.save(dir + "/emb." + to_string(i) + ".mat");

    if (decay != 0 && !lr_decay && dev_err - dev_err_new < 0.003) {
      lr_decay = true;
      lr_decay_epoch = (double)i;
    }

    if (decay != 0 && !lr_decay)
      dev_err = dev_err_new;

    if (eval23)
      eval_super(test_words, test_labels, false, h_tm1, wx, wh,
          wy, suffix, cap, emb, cs, ws, vocsize, suffix_count, y, ntokens23);

    cout << endl;
    fflush(stdout);
  }

  return 0;
}
